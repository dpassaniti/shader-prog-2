//gemetry shader, split a mesh into triangles and slowly pulse periodically

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer manipBuffer : register(cb1)
{
	float time;
	float3 padding;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXCOORD0;
};

[maxvertexcount(3)]
void main(triangle InputType input[3], inout TriangleStream<OutputType> triStream)
{
	OutputType output;

	//scaling matrix
	float4x4 scaleMatrix =
	{
		{ 3, 0, 0, 0 },
		{ 0, 3, 0, 0 },
		{ 0, 0, 3, 0 },
		{ 0, 0, 0, 1 }
	};

	//calculate face normal
	float3 side1 = input[0].position - input[1].position;
	float3 side2 = input[2].position - input[1].position;
	float3 faceNormal = normalize(cross(side1, side2));

	//for every triangle, output a triangle
	for (int i = 0; i < 3; i++)
	{
		//MANIP
		//copy info
		output.position = input[i].position;
		//scale
		output.position = mul(output.position, scaleMatrix);
		//explode
		output.position += sin(time / 2 + float4(faceNormal * 10, 1.0f)) / 2;

		//FINALISE OUTPUT
		// Calculate the position of the vertex against the world, view, and projection matrices.
		output.position = mul(output.position, worldMatrix);
		output.position = mul(output.position, viewMatrix);
		output.position = mul(output.position, projectionMatrix);
			
		//depth pos
		output.depthPosition = output.position;

		triStream.Append(output);
	}
	triStream.RestartStrip();
}