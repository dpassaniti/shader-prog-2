//water pixel shader

#define N_OF_LIGHTS 3

Texture2D texture0 : register(t0);
Texture2D depthMapTexture1 : register(t1);
Texture2D depthMapTexture2 : register(t2);
Texture2D depthMapTexture3 : register(t3);

SamplerState Sampler0 : register(s0);
SamplerState SampleTypeClamp : register(s1);

cbuffer LightBuffer : register(cb0)
{
	float4 lightPosition[N_OF_LIGHTS];
	float4 specularColour[N_OF_LIGHTS];
	float4 diffuseColour[N_OF_LIGHTS];
	float4 ambientColour[N_OF_LIGHTS];
	float4 specularPower[N_OF_LIGHTS];
	float4 fallOffDistance[N_OF_LIGHTS];
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 position3D : TEXCOORD2;
	float3 viewDirection : TEXCOORD1;
	float4 lightViewPosition[N_OF_LIGHTS] : TEXCOORD3;
};


float4 main(InputType input) : SV_TARGET
{
	float lightIntensity;//check if we need to affect value at all
	float4 specularIntensity;//perfect mirror has highest,which means smallest highlight
	float3 lightDir;//vector from light to vertex(for point light and attenuation calc)
	float3 reflection;//for specular calculation
	float4 textureColour;//sampled pixel from texture
	float4 diffuseComponent;//diffuse colour
	float4 specularComponent;//specular highlights
	float attenuation;//fall off factor
	float4 finalColour;//final output

	Texture2D depthMapTextures[N_OF_LIGHTS];

	depthMapTextures[0] = depthMapTexture1;
	depthMapTextures[1] = depthMapTexture2;
	depthMapTextures[2] = depthMapTexture3;

	float bias;//bias to avoid aliasing artefacts
	float2 projectTexCoord;//shadow
	float depthValue;//depth check
	float lightDepthValue;//depth check
	// Set the bias value for fixing the floating point precision issues.
	bias = 0.001f;

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColour = texture0.Sample(Sampler0, input.tex);

	// Set the default output color to the ambient light value for all pixels.
	finalColour = ambientColour[0];

	//add texture pixel colour
	finalColour = finalColour * textureColour;

	//for each light
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		// Calculate the projected texture coordinates.
		projectTexCoord.x = input.lightViewPosition[i].x / input.lightViewPosition[i].w / 2.0f + 0.5f;
		projectTexCoord.y = -input.lightViewPosition[i].y / input.lightViewPosition[i].w / 2.0f + 0.5f;

		// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
		if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
		{
			// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
			depthValue = depthMapTextures[i].Sample(SampleTypeClamp, projectTexCoord).r;

			// Calculate the depth of the light.
			lightDepthValue = input.lightViewPosition[i].z / input.lightViewPosition[i].w;

			// Subtract the bias from the lightDepthValue.
			lightDepthValue = lightDepthValue - bias;

			// Compare the depth of the shadow map value and the depth of the light to determine whether to shadow or to light this pixel.
			// If the light is in front of the object then light the pixel, if not then shadow this pixel since an object (occluder) is casting a shadow on it.
			if (lightDepthValue < depthValue)
			{
				//normalize distance of pixel from light source to get the direction
				lightDir = normalize(input.position3D - lightPosition[i]);
				// Calculate the amount of light on this pixel
				lightIntensity = saturate(dot(input.normal, -lightDir));
				//if the pixel is affected by light, calculate and add diffuse and specular components
				if (lightIntensity > 0.0f)
				{
					//diffuse
					// Determine the final diffuse colour based on the diffuse color and the amount of light intensity.
					diffuseComponent = diffuseColour[i] * lightIntensity;

					//specular
					//get reflection vector
					reflection = reflect(lightDir, input.normal);
					//get specular intensity based on reflection vector
					specularIntensity = pow(saturate(dot(reflection, input.viewDirection)), specularPower[i].x);
					//determine specular comp�onent based on specular colour and specular intensity
					specularComponent = specularColour[i] * specularIntensity;

					//attenuation
					//reduce intensity based on distance
					//linear attenuation factor = 1 - (distanceFromLight/fallOffDistance)
					//saturate value because fallOffDistance could be bigger than distanceFromLight
					attenuation = 1.0f - saturate(length(input.position3D - lightPosition[i]) / fallOffDistance[i].x);
					finalColour += (diffuseComponent + specularComponent)*attenuation;
				}
			}
		}
	}

	// Saturatefinal colour.	
	finalColour = saturate(finalColour);
	//finalColour.w = 0.1;
	return finalColour;
}