//geometry shader, for for each input triangle, create 9 points and chaotically explode them outward
//points correspond to the the triangle's vertices scaled by a factor of 1(first 3 points), 2(second 3 points) and 3(third 3 points)

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer manipBuffer : register(cb1)
{
	float3 cameraPosition;
	float time;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 position3D : TEXCOORD2;
	float3 viewDirection : TEXCOORD1;
};

[maxvertexcount(9)]
void main(triangle InputType input[3], inout PointStream<OutputType> pointStream)
{
	OutputType output;
	
	//calculate triangle's face normal
	float3 side1 = input[0].position - input[1].position;
	float3 side2 = input[2].position - input[1].position;
	float3 faceNormal = normalize(cross(side1, side2));

	for (int i = 0; i < 3; i++)//for every vertex of the triangle
	{
		//create 3 points
		for (int j = 1; j < 4; ++j)//start at 1 to avoid scaling to 0
		{
			float4x4 scaleMatrix =
			{
				{ j, 0, 0, 0 },
				{ 0, j, 0, 0 },
				{ 0, 0, j, 0 },
				{ 0, 0, 0, 1 }
			};

			//MANIP
			//"scale" position
			output.position = mul(input[i].position, scaleMatrix);
			//translate along triangle's face normal, 
			output.position += -(10-time) * float4(faceNormal, 1.0f);

			//FINALISE OUTPUT
			// world position of vertex
			output.position3D = mul(output.position, worldMatrix);
			// Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
			output.viewDirection.x = cameraPosition.x - output.position3D.x;
			output.viewDirection.y = cameraPosition.y - output.position3D.y;
			output.viewDirection.z = cameraPosition.z - output.position3D.z;
			// Calculate the position of the vertex against the world, view, and projection matrices.
			output.position = mul(output.position, worldMatrix);
			output.position = mul(output.position, viewMatrix);
			output.position = mul(output.position, projectionMatrix);
			//texture coord
			output.tex = input[i].tex;
			//normal
			output.normal = mul(input[i].normal, (float3x3)worldMatrix);
			output.normal = normalize(output.normal);
			pointStream.Append(output);
		}
	}
	pointStream.RestartStrip();
}