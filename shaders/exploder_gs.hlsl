//gemetry shader, split a mesh into triangles and slowly pulse periodically

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer manipBuffer : register(cb1)
{
	float3 cameraPosition;
	float time;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 position3D : TEXCOORD2;
	float3 viewDirection : TEXCOORD1;
};

[maxvertexcount(3)]
void main(triangle InputType input[3], inout TriangleStream<OutputType> triStream)
{
	OutputType output;

	//scaling matrix
	float4x4 scaleMatrix =
	{
	{ 3, 0, 0, 0 },
	{ 0, 3, 0, 0 },
	{ 0, 0, 3, 0 },
	{ 0, 0, 0, 1 }
	};
	
	//calculate face normal
	float3 side1 = input[0].position - input[1].position;
	float3 side2 = input[2].position - input[1].position;
	float3 faceNormal = normalize(cross(side1, side2));

	//for every triangle, output a triangle
	for (int i = 0; i < 3; i++)
	{
		//MANIP
		//copy info
		output.position = input[i].position;
		//scale
		output.position = mul(output.position, scaleMatrix);
		//explode
		output.position += sin(time/2 + float4(faceNormal *10, 1.0f))/2;

		//FINALISE OUTPUT
		// world position of vertex
		output.position3D = mul(output.position, worldMatrix);
		// Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
		output.viewDirection.x = cameraPosition.x - output.position3D.x;
		output.viewDirection.y = cameraPosition.y - output.position3D.y;
		output.viewDirection.z = cameraPosition.z - output.position3D.z;	
		// Calculate the position of the vertex against the world, view, and projection matrices.
		output.position = mul(output.position, worldMatrix);
		output.position = mul(output.position, viewMatrix);
		output.position = mul(output.position, projectionMatrix);
		//texture coord
		output.tex = input[i].tex;
		//normal
		output.normal = mul(input[i].normal, (float3x3)worldMatrix);
		output.normal = normalize(output.normal);
		triStream.Append(output);
	}
	triStream.RestartStrip();
}