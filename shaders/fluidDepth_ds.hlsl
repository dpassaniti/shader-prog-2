// Fluid domain shader
// After tessellation the domain shader processes all the vertices
// applies wave deformer

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer ManipBuffer : register(cb1)
{
	float waveFrequency;
	float time;
	int size;
	float waveHeight;
};

struct ConstantOutputType
{
	float edges[4] : SV_TessFactor;
	float inside[2] : SV_InsideTessFactor;
};

struct InputType
{
	float3 position : POSITION;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXCOORD0;
};

[domain("quad")]
OutputType main(ConstantOutputType input, float2 uvwCoord : SV_DomainLocation, const OutputPatch<InputType, 4> patch)
{
	float3 vertexPosition;
	OutputType output;

	// Determine the position of the new vertex by interpolation
	//care!!!the ordering of control points depends on the order in which the quad verteces are drawn in the mesh
	float3 v1 = lerp(patch[0].position, patch[1].position, 1 - uvwCoord.y);
	float3 v2 = lerp(patch[2].position, patch[3].position, 1 - uvwCoord.y);
	vertexPosition = lerp(v1, v2, uvwCoord.x);

	//MANIPULATION

	//deform vertex
	//patch is drawn with corner in origin
	//correct coordinates so the calculation will have the waves spawn in the center of the patch
	float ex = vertexPosition.x - size / 2;//match with origin
	float zed = vertexPosition.z - size / 2;//match with origin
	//get new coordinates based on wave function F(x,y,z)[x, sin(frequency * sqrt(x^2 + z^2) + time) ,z]
	vertexPosition.y += waveHeight*sin(waveFrequency * sqrt(ex*ex + zed*zed) - time / 2);//-time to have wave direction going outward

	//FINALISE OUTPUT
	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(float4(vertexPosition, 1.0f), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	// Store the position value in a second input value for depth value calculations.
	output.depthPosition = output.position;

	return output;
}
