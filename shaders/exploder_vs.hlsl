//vertex buffer, rotates vertex in x,y,z plane by angle of time/10

cbuffer manipBuffer : register(cb0)
{
	float3 cameraPosition;//not used
	float time;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};


InputType main(InputType input)
{
	OutputType output;
	
	//copy data
	output.position = input.position;
	output.tex = input.tex;
	output.normal = input.normal;

	//calculate vertex rotation
	float slowTime = time / 10;
	//rotation on x axis
	float4x4 rotateXMatrix =
	{
		{ 1, 0, 0, 0 },
		{ 0, cos(slowTime), -sin(slowTime), 0 },
		{ 0, sin(slowTime), cos(slowTime), 0 },
		{ 0, 0, 0, 1 }
	};
	//rotation on z axis
	float4x4 rotateZMatrix =
	{
		{ cos(slowTime), -sin(slowTime), 0, 0 },
		{ sin(slowTime), cos(slowTime), 0, 0 },
		{ 0, 0, 1, 0 },
		{ 0, 0, 0, 1 }
	};
	//rotation on y axis
	float4x4 rotateYMatrix =
	{
		{ cos(slowTime), 0, sin(slowTime), 0 },
		{ 0, 1, 0, 0 },
		{ -sin(slowTime), 0, cos(slowTime), 0 },
		{ 0, 0, 0, 1 }
	};

	float4x4 finalRotation = mul(mul(rotateXMatrix, rotateZMatrix), rotateYMatrix);	
	output.position = mul(output.position, finalRotation);

	return output;
}