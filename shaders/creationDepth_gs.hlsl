//geometry shader, for for each input triangle, create 9 points and slowly move them in the opposite direction of the triangle face normal
//points correspond to the the triangle's vertices scaled by a factor of 1(first 3 points), 2(second 3 points) and 3(third 3 points) 

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer manipBuffer : register(cb1)
{
	float time;
	float3 padding;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXCOORD0;
};

[maxvertexcount(9)]
void main(triangle InputType input[3], inout PointStream<OutputType> pointStream)
{
	OutputType output;

	//calculate triangle's face normal
	float3 side1 = input[0].position - input[1].position;
	float3 side2 = input[2].position - input[1].position;
	float3 faceNormal = normalize(cross(side1, side2));

	for (int i = 0; i < 3; i++)//for every vertex of the triangle
	{
		//create 3 points
		for (int j = 1; j < 4; ++j)//start at 1 to avoid scaling to 0
		{
			float4x4 scaleMatrix =
			{
				{ j, 0, 0, 0 },
				{ 0, j, 0, 0 },
				{ 0, 0, j, 0 },
				{ 0, 0, 0, 1 }
			};

			//MANIP
			//"scale" position
			output.position = mul(input[i].position, scaleMatrix);
			//translate along triangle's face normal, 
			output.position += time * float4(faceNormal, 0.01f);

			//FINALISE OUTPUT
			// Calculate the position of the vertex against the world, view, and projection matrices.
			output.position = mul(output.position, worldMatrix);
			output.position = mul(output.position, viewMatrix);
			output.position = mul(output.position, projectionMatrix);
			//depth pos
			output.depthPosition = output.position;

			pointStream.Append(output);
		}
	}
	pointStream.RestartStrip();
}