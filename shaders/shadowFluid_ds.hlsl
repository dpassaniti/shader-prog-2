// Fluid domain shader
// After tessellation the domain shader processes all the vertices
// redefines normals and texture coordinates, applies wave deformer

#define N_OF_LIGHTS 3

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix lightViewMatrix[N_OF_LIGHTS];
	matrix lightProjectionMatrix[N_OF_LIGHTS];
};

cbuffer ManipBuffer : register(cb1)
{
	float3 cameraPosition;
	float waveFrequency;
	float time;
	int size;
	float tiles;
	float waveHeight;
};

struct ConstantOutputType
{
	float edges[4] : SV_TessFactor;
	float inside[2] : SV_InsideTessFactor;
};

struct InputType
{
	float3 position : POSITION;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 position3D : TEXCOORD2;
	float3 viewDirection : TEXCOORD1;
	float4 lightViewPosition[N_OF_LIGHTS] : TEXCOORD3;
};

[domain("quad")]
OutputType main(ConstantOutputType input, float2 uvwCoord : SV_DomainLocation, const OutputPatch<InputType, 4> patch)
{
	float3 vertexPosition;
	OutputType output;
	float3 newNormal = { 0.0f, 1.0f, 0.0f };//input mesh is a plane in xz, so start calculations from 0,1,0

		// Determine the position of the new vertex by interpolation
		//care!!!the ordering of control points depends on the order in which the quad verteces are drawn in the mesh
		float3 v1 = lerp(patch[0].position, patch[1].position, 1 - uvwCoord.y);
		float3 v2 = lerp(patch[2].position, patch[3].position, 1 - uvwCoord.y);
		vertexPosition = lerp(v1, v2, uvwCoord.x);

	//MANIPULATION

	//deform vertex
	//patch is drawn with corner in origin
	//correct coordinates so the calculation will have the waves spawn in the center of the patch
	float ex = vertexPosition.x - size / 2;//match with origin
	float zed = vertexPosition.z - size / 2;//match with origin
	//get new coordinates based on wave function F(x,y,z)[x, sin(frequency * sqrt(x^2 + z^2) + time) ,z]
	vertexPosition.y += waveHeight*sin(waveFrequency * sqrt(ex*ex + zed*zed) - time / 2);//-time to have wave direction going outward

	//deform normal
	//when calculating the normal of a vertex of exact position x=size/2 z=size/2,
	//keep the normal unchanged, or it will be set to 0,0,0 due to the position correction applied before.
	if (!(vertexPosition.x == size / 2 && vertexPosition.z == size / 2))
	{
		//compute jacobian matrix
		float step = cos(waveFrequency*sqrt(ex*ex + zed*zed) - time / 2) / sqrt(ex*ex + zed*zed);
		float3x3 jacobianM =
		{
			{ 1, 0, 0 },
			{ waveHeight*step*waveFrequency.x*ex, 1, waveHeight*step*waveFrequency.x*zed },
			{ 0, 0, 1 }
		};
		//tangent
		float3 tangent;
		float3 temp1 = { 0.0f, 0.0f, 1.0f };
			float3 temp2 = { 0.0f, 1.0f, 0.0f };
			//pick cross product with higher magnitude
			temp1 = cross(newNormal, temp1);
		temp2 = cross(newNormal, temp2);
		if (length(temp1) > length(temp2))
			tangent = temp1;
		else
			tangent = temp2;
		//binormal
		float3 binormal = cross(newNormal, tangent);
			//normal
			newNormal = normalize(cross(mul(jacobianM, tangent), mul(jacobianM, binormal)));
	}

	//FINALISE OUTPUT
	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(float4(vertexPosition, 1.0f), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	//calculate texture coordinates based on new position and total size of patch
	//divide size by n to tile n times
	output.tex.x = vertexPosition.x / (size / tiles);
	output.tex.y = -vertexPosition.z / (size / tiles);

	// Calculate the normal vector against the world matrix only.
	output.normal = mul(newNormal, (float3x3)worldMatrix);
	// Normalize the normal vector.
	output.normal = normalize(output.normal);

	// world position of vertex
	output.position3D = mul(vertexPosition, worldMatrix);
	// Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
	output.viewDirection.x = cameraPosition.x - output.position3D.x;
	output.viewDirection.y = cameraPosition.y - output.position3D.y;
	output.viewDirection.z = cameraPosition.z - output.position3D.z;
	// Normalize the viewing direction vector.
	output.viewDirection = normalize(output.viewDirection);

	//SHADOW
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		// Calculate the position of the vertex as viewed by the light source.
		output.lightViewPosition[i] = mul(float4(vertexPosition, 1.0f), worldMatrix);
		output.lightViewPosition[i] = mul(output.lightViewPosition[i], lightViewMatrix[i]);
		output.lightViewPosition[i] = mul(output.lightViewPosition[i], lightProjectionMatrix[i]);
	}
	
	return output;
}

