#pragma once
#include "BaseShader.h"
#include "Light.h"
#include "Camera.h"
#include "N_OF_LIGHTS.h"

using namespace std;
using namespace DirectX;

class ExploderShader : public BaseShader
{

	//buffers types
	//multiples of 16: 16,32,48,64,80,96,112,128,144,160,176,192,208,224,240,256,272,288,304,320
	struct LightBuffer//lights description
	{
		XMFLOAT4 lightPosition[N_OF_LIGHTS];
		XMFLOAT4 specularColour[N_OF_LIGHTS];
		XMFLOAT4 diffuseColour[N_OF_LIGHTS];
		XMFLOAT4 ambientColour[N_OF_LIGHTS];
		XMFLOAT4 specularPower[N_OF_LIGHTS];//waste of bytes but padding every time n of lights changes is hard
		XMFLOAT4 fallOffDistance[N_OF_LIGHTS];//^^
	};

	struct ManipBuffer//geometry and vertex manipulation;
	{
		XMFLOAT3 cameraPosition;
		float time;
	};

public:
	ExploderShader(ID3D11Device* device, HWND hwnd, WCHAR* gsPath);//path to geometry shader for 3 different animations
	~ExploderShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		ID3D11ShaderResourceView* texture, Light* lights[N_OF_LIGHTS], Camera* camera, float time);
	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
	void InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* m_matrixBuffer;
	ID3D11Buffer* manipBfr;
	ID3D11Buffer* lightBfr;
	ID3D11SamplerState* m_sampleState;
};