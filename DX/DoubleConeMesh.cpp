#include "DoubleConeMesh.h"

//TODO: fix normals

DoubleConeMesh::DoubleConeMesh(ID3D11Device* device, WCHAR* textureFilename, int resolution)
{
	m_resolution = resolution;
	// Initialize the vertex and index buffer that hold the geometry for the triangle.
	InitBuffers(device);

	// Load the texture for this model.
	LoadTexture(device, textureFilename);
}


DoubleConeMesh::~DoubleConeMesh()
{
	// Run parent deconstructor
	BaseMesh::~BaseMesh();
}


void DoubleConeMesh::InitBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	// Calculate vertex count
	// Set the number of vertices in the vertex array.
	m_vertexCount = 2*((6 * m_resolution)*m_resolution);

	// Set the number of indices in the index array.
	m_indexCount = m_vertexCount;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];

	// Create the index array.
	indices = new unsigned long[m_indexCount];

	// distance increase at each step
	float xStep = XM_2PI / m_resolution;
	float yStep = 3.0f / m_resolution;//height/resolution
	float zStep = xStep;
	//texture uv
	float uStep = 1.0f / m_resolution;
	float vStep = 1.0f / m_resolution;
		
	//Counters
	int v = 0;	// vertex counter
	int i = 0;	// index counter
		
	for (float vertical = 0; vertical < m_resolution; vertical++)	// build from base up
	{
		float r1 = ((m_resolution - vertical) / m_resolution);//current lower radius
		float r2 = ((m_resolution - vertical - 1) / m_resolution);//current upper radius
		for (float horizontal = 0; horizontal < m_resolution; horizontal++)	// build 1 horizontal section
		{
			//UPPER CONE
			//0
			vertices[v].position = XMFLOAT3(r1*cos(xStep*horizontal), yStep*vertical, r1*sin(zStep*horizontal));  // Bottom left.
			vertices[v].texture = XMFLOAT2(uStep*horizontal, vStep*vertical);
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;

			//1
			vertices[v].position = XMFLOAT3(r2*cos(xStep*(horizontal + 1)), yStep*(vertical + 1), r2*sin(zStep*(horizontal + 1)));  // Top right.
			vertices[v].texture = XMFLOAT2(uStep*(horizontal+1), vStep*(vertical+1));
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;

			//2
			vertices[v].position = XMFLOAT3(r2*cos(xStep*horizontal), yStep*(vertical + 1), r2*sin(zStep*horizontal));  // Top left.
			vertices[v].texture = XMFLOAT2(uStep*horizontal, vStep*(vertical + 1));
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;
			
			//0
			vertices[v].position = XMFLOAT3(r1*cos(xStep*horizontal), yStep*vertical, r1*sin(zStep*horizontal));  // Bottom left.
			vertices[v].texture = XMFLOAT2(uStep*horizontal, vStep*vertical);
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;
			
			//3
			vertices[v].position = XMFLOAT3(r1*cos(xStep*(horizontal + 1)), yStep*vertical, r1*sin(zStep*(horizontal + 1)));  // Bottom right.
			vertices[v].texture = XMFLOAT2(uStep*(horizontal+1), vStep*vertical);
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;

			//1
			vertices[v].position = XMFLOAT3(r2*cos(xStep*(horizontal + 1)), yStep*(vertical + 1), r2*sin(zStep*(horizontal + 1)));  // Top right.
			vertices[v].texture = XMFLOAT2(uStep*(horizontal + 1), vStep*(vertical + 1));
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;

			//LOWER CONE
			//like upper cone, buf invert drawing order and y coordinate
			
			//2
			vertices[v].position = XMFLOAT3(r2*cos(xStep*horizontal), -yStep*(vertical + 1), r2*sin(zStep*horizontal));  // Top left.
			vertices[v].texture = XMFLOAT2(uStep*horizontal, vStep*(vertical + 1));
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;
			

			//1
			vertices[v].position = XMFLOAT3(r2*cos(xStep*(horizontal + 1)), -yStep*(vertical + 1), r2*sin(zStep*(horizontal + 1)));  // Top right.
			vertices[v].texture = XMFLOAT2(uStep*(horizontal + 1), vStep*(vertical + 1));
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;

			//0
			vertices[v].position = XMFLOAT3(r1*cos(xStep*horizontal), -yStep*vertical, r1*sin(zStep*horizontal));  // Bottom left.
			vertices[v].texture = XMFLOAT2(uStep*horizontal, vStep*vertical);
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;

			
			//1
			vertices[v].position = XMFLOAT3(r2*cos(xStep*(horizontal + 1)), -yStep*(vertical + 1), r2*sin(zStep*(horizontal + 1)));  // Top right.
			vertices[v].texture = XMFLOAT2(uStep*(horizontal + 1), vStep*(vertical + 1));
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;
			//3
			vertices[v].position = XMFLOAT3(r1*cos(xStep*(horizontal + 1)), -yStep*vertical, r1*sin(zStep*(horizontal + 1)));  // Bottom right.
			vertices[v].texture = XMFLOAT2(uStep*(horizontal + 1), vStep*vertical);
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;
			//0
			vertices[v].position = XMFLOAT3(r1*cos(xStep*horizontal), -yStep*vertical, r1*sin(zStep*horizontal));  // Bottom left.
			vertices[v].texture = XMFLOAT2(uStep*horizontal, vStep*vertical);
			//vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			v++;
			indices[i] = i;
			i++;
			
		}
	}
	

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType)* m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long)* m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;
}

