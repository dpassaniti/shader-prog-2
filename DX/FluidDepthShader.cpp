//dynamic planar fluid with depth as colour

#include "FluidDepthShader.h"

FluidDepthShader::FluidDepthShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
{
	InitShader(L"../shaders/fluid_vs.hlsl", L"../shaders/fluid_hs.hlsl", L"../shaders/fluidDepth_ds.hlsl", L"../shaders/depth_ps.hlsl");
}

FluidDepthShader::~FluidDepthShader()
{

	// Release the matrix constant buffer.
	if (m_matrixBuffer)
	{
		m_matrixBuffer->Release();
		m_matrixBuffer = 0;
	}

	// Release the layout.
	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	if (manipBfr)
	{
		manipBfr->Release();
		manipBfr = 0;
	}
	if (tessellationBfr)
	{
		tessellationBfr->Release();
		tessellationBfr = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void FluidDepthShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
{
	D3D11_BUFFER_DESC matrixBufferDesc, manipBfrDesc, tessellationBfrDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

	//setup manip buffer
	manipBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	manipBfrDesc.ByteWidth = sizeof(ManipBuffer);
	manipBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	manipBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	manipBfrDesc.MiscFlags = 0;
	manipBfrDesc.StructureByteStride = 0;
	//create the constant buffer pointer
	m_device->CreateBuffer(&manipBfrDesc, NULL, &manipBfr);

	//setup tessellation buffer
	tessellationBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	tessellationBfrDesc.ByteWidth = sizeof(TessellationBuffer);
	tessellationBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	tessellationBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	tessellationBfrDesc.MiscFlags = 0;
	tessellationBfrDesc.StructureByteStride = 0;
	// Create the constant buffer pointer
	m_device->CreateBuffer(&tessellationBfrDesc, NULL, &tessellationBfr);
}

void FluidDepthShader::InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename)
{
	// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
	InitShader(vsFilename, psFilename);

	// Load other required shaders.
	loadHullShader(hsFilename);
	loadDomainShader(dsFilename);
}

void FluidDepthShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,
	const XMMATRIX &worldMatrix, const XMMATRIX &viewMatrix, const XMMATRIX &projectionMatrix,
	float frequency, float time, float tessFactor, int size, float height)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	XMMATRIX tworld, tview, tproj;

	//buffer pointers
	MatrixBufferType* dataPtr;
	ManipBuffer* manipPtr;
	TessellationBuffer* tessellationPtr;
	//slot
	unsigned int bufferNumber;

	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(worldMatrix);
	tview = XMMatrixTranspose(viewMatrix);
	tproj = XMMatrixTranspose(projectionMatrix);

	//HULL SHADER DATA
	deviceContext->Map(tessellationBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	tessellationPtr = (TessellationBuffer*)mappedResource.pData;
	tessellationPtr->tessellationFactor = tessFactor;
	deviceContext->Unmap(tessellationBfr, 0);
	bufferNumber = 0;
	deviceContext->HSSetConstantBuffers(bufferNumber, 1, &tessellationBfr);

	//DOMAIN SHADER DATA
	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	deviceContext->Unmap(m_matrixBuffer, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Now set the constant buffer in the vertex shader with the updated values.
	deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

	//Send manip to domain shader
	deviceContext->Map(manipBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	manipPtr = (ManipBuffer*)mappedResource.pData;
	manipPtr->waveFrequency = frequency;
	manipPtr->time = time;
	manipPtr->size = size;
	manipPtr->waveHeight = height;
	deviceContext->Unmap(manipBfr, 0);
	bufferNumber = 1;
	deviceContext->DSSetConstantBuffers(bufferNumber, 1, &manipBfr);
}

void FluidDepthShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
{
	// Base render function.
	BaseShader::Render(deviceContext, indexCount);
}


