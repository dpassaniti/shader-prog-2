//dynamic planar fluid with depth as colour

#pragma once
#include "BaseShader.h"
#include "Light.h"
#include "Camera.h"

using namespace std;
using namespace DirectX;

class FluidDepthShader : public BaseShader
{
	struct TessellationBuffer//tessellation factor
	{
		float tessellationFactor;
		XMFLOAT3 padding;
	};

	struct ManipBuffer//data for vertex manipulation/orientation
	{
		float waveFrequency;
		float time;
		int size;//size of patch
		float waveHeight;
	};

public:
	FluidDepthShader(ID3D11Device* device, HWND hwnd);
	~FluidDepthShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext,
		const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		float frequency, float time, float tessFactor, int size, float height);

	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
	void InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);
	ID3D11Buffer* m_matrixBuffer;

	//buffers
	ID3D11Buffer* manipBfr;
	ID3D11Buffer* tessellationBfr;
};
