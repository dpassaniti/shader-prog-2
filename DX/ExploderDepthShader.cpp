#include "ExploderDepthShader.h"


ExploderDepthShader::ExploderDepthShader(ID3D11Device* device, HWND hwnd, WCHAR* gsPath) : BaseShader(device, hwnd)
{
	InitShader(L"../shaders/exploder_vs.hlsl", gsPath, L"../shaders/depth_ps.hlsl");
}


ExploderDepthShader::~ExploderDepthShader()
{
	// Release the sampler state.
	if (m_sampleState)
	{
		m_sampleState->Release();
		m_sampleState = 0;
	}

	// Release the matrix constant buffer.
	if (m_matrixBuffer)
	{
		m_matrixBuffer->Release();
		m_matrixBuffer = 0;
	}

	// Release the layout.
	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void ExploderDepthShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
{
	D3D11_BUFFER_DESC matrixBufferDesc, manipBufferDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	manipBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	manipBufferDesc.ByteWidth = sizeof(ManipBuffer);
	manipBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	manipBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	manipBufferDesc.MiscFlags = 0;
	manipBufferDesc.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&manipBufferDesc, NULL, &manipBfr);

}

void ExploderDepthShader::InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename)
{
	// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
	InitShader(vsFilename, psFilename);

	// Load other required shaders.
	loadGeometryShader(gsFilename);
}


void ExploderDepthShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &worldMatrix, const XMMATRIX &viewMatrix, const XMMATRIX &projectionMatrix, float time)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;
	ManipBuffer* manipPtr;

	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(worldMatrix);
	tview = XMMatrixTranspose(viewMatrix);
	tproj = XMMatrixTranspose(projectionMatrix);

	//VERTEX SHADER DATA
	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	deviceContext->Unmap(m_matrixBuffer, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Now set the constant buffer in the vertex shader with the updated values.
	deviceContext->GSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

	//SEND MANIP DATA TO BOTH VERTEX AND GEOMETRY
	deviceContext->Map(manipBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	manipPtr = (ManipBuffer*)mappedResource.pData;
	manipPtr->time = time;
	deviceContext->Unmap(manipBfr, 0);
	bufferNumber = 1;
	deviceContext->GSSetConstantBuffers(bufferNumber, 1, &manipBfr);//send to geometry shader
	bufferNumber = 0;
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &manipBfr);//send to vertex shader

}

void ExploderDepthShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
{
	// Base render function.
	BaseShader::Render(deviceContext, indexCount);
}



