#pragma once
//includes
#include <vector>
#include "baseapplication.h"
#include "Texture.h"
#include "Light.h"
#include "RenderTexture.h"

//shaders
#include "TextureShader.h"
#include "FluidShader.h"
#include "ExploderShader.h"
#include "DimShader.h"
#include "HorizontalBlurShader.h"
#include "VerticalBlurShader.h"
#include "LavaLampShader.h"
#include "FluidDepthShader.h"
#include "ExploderDepthShader.h"
#include "ShadowFluidShader.h"

//meshes
#include "QuadCPPatch.h"
#include "SphereTriList.h"
#include "OrthoMesh.h"
#include "Skybox.h"
#include "DoubleConeMesh.h"

//defines
#include "N_OF_LIGHTS.h"

#define LIGHT0_POSITION 20.0f, 5.0f, 20.0f
#define LIGHT1_POSITION 70.0f, 15.0f, 40.0f
#define LIGHT2_POSITION 50.0f, 30.0f, 55.0f

#define LIGHT0_COLOUR 0.3f, 0.0f, 0.8f, 1.0f//purple
#define LIGHT1_COLOUR 0.0f, 0.7f, 1.0f, 1.0f//light blue
#define LIGHT2_COLOUR 0.8f, 0.5f, 0.2f, 1.0f//orange

#define SHADOWMAP_SIZE 1024

#define CC 0.0f, 0.0f, 0.0f, 0.0f//clear colour for secondary render targets

class Coursework : public BaseApplication
{
public:
	Coursework(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input*);
	~Coursework();
	bool Frame();

	//exploder
	void renderExploder(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix);
	//glow post processing
	void lightsToTexture(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix);
	void addGlowTrail(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);
	void hBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);
	void vBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);	
	void drawGlowFilter(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);
	//shadows
	void createShadowMaps(XMMATRIX* worldMatrix);
	void renderExploderDepth(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix);

private:
	void HandleInput(float frameTime);
	bool Render();

	//containers
	std::vector<BaseShader*> shaders;
	std::vector<BaseMesh*> meshes;
	std::vector<Texture*> textures;
	std::vector<RenderTexture*> renderTargets;

	//lights
	Light* lights[N_OF_LIGHTS];
	Light* lightZero;
	Light* lightOne;
	Light* lightTwo;

	//shaders
	TextureShader* textureS;
	FluidShader* fluidS;
	ExploderShader* exploderS;
	ExploderShader* creationS;
	ExploderShader* destructionS;
	DimShader* dimS;
	LavaLampShader* lavaLampS;
	HorizontalBlurShader* hBlurS;
	VerticalBlurShader* vBlurS;
	FluidDepthShader* fluidDepthS;
	ExploderDepthShader* exploderDepthS;
	ExploderDepthShader* creationDepthS;
	ExploderDepthShader* destructionDepthS;
	ShadowFluidShader* shadowFluidS;

	//meshes
	QuadCPPatch* fluid;
	DoubleConeMesh* lamp;
	SphereTriList* exploder;
	OrthoMesh* glowFilter;//post process
	Skybox* skybox;
	
	//textures
	Texture* fluidTex;
	Texture* lightTex;
	Texture* exploderTex;
	Texture* skyboxTex;

	//secondary render targets
	RenderTexture* glowTargetA;
	RenderTexture* glowTargetB;
	RenderTexture* previousGlow;
	RenderTexture* shadowMapZero;//for light 0
	RenderTexture* shadowMapOne;//for light 1
	RenderTexture* shadowMapTwo;//for light 2
	RenderTexture* shadowTargets[N_OF_LIGHTS];
	ID3D11ShaderResourceView* shadowMaps[N_OF_LIGHTS];

	//flags and variables
	bool wireframe;//on\off
	float time;//for dynamic manipulation
	float frequency;//wave frequency in fluid
	float waveHeight;//height of fluid's waves
	float tessellation;//tessellation factor, 1 to 64
	float tiling;//texture tiling of fluid		
	float timer;//for esploder animations
	int exploderState;//for esploder animations
	float sWidth, sHeight;//screen dimensions
	float dimStrength;//brightness of glow effect
	int currentLight;//keep track of selected light
	float fallOffs[N_OF_LIGHTS];//remember light fall off when turning it off
	bool shadows;//render shadows or not
};