#include "Coursework.h"

Coursework::Coursework(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input *in) : BaseApplication(hinstance, hwnd, screenWidth, screenHeight, in)
{
	//init vars
	sWidth = screenWidth;
	sHeight = screenHeight;
	wireframe = false;
	frequency = 0.7f;
	waveHeight = 0.3f;
	tessellation = 64;
	tiling = 2;
	exploderState = 0;
	dimStrength = 0.6;
	currentLight = 0;
	shadows = false;

	//reposition camera
	m_Camera->SetPosition(50.0f, 10.0f, -15.0f);	

	//create and init lights

	lightZero = new Light();
	lightZero->SetPosition(LIGHT0_POSITION);
	lightZero->SetLookAt(50.0f, 0.0f, 50.0f);
	lightZero->SetAmbientColour(0.5f, 0.5f, 0.5f, 0.5f);
	lightZero->SetDiffuseColour(LIGHT0_COLOUR);
	lightZero->SetSpecularColour(1.0f, 1.0f, 1.0f, 1.0f);
	lightZero->SetSpecularPower(20);
	lightZero->SetFallOffDistance(70);

	lightOne = new Light();
	lightOne->SetPosition(LIGHT1_POSITION);
	lightOne->SetLookAt(50.0f, 0.0f, 50.0f);
	lightOne->SetAmbientColour(0.5f, 0.5f, 0.5f, 0.5f);
	lightOne->SetDiffuseColour(LIGHT1_COLOUR);
	lightOne->SetSpecularColour(1.0f, 1.0f, 1.0f, 1.0f);
	lightOne->SetSpecularPower(80);
	lightOne->SetFallOffDistance(50);

	lightTwo = new Light();
	lightTwo->SetPosition(LIGHT2_POSITION);
	lightTwo->SetLookAt(50.0f, 0.0f, 50.0f);
	lightTwo->SetAmbientColour(0.5f, 0.5f, 0.5f, 0.5f);
	lightTwo->SetDiffuseColour(LIGHT2_COLOUR);
	lightTwo->SetSpecularColour(1.0f, 1.0f, 1.0f, 1.0f);
	lightTwo->SetSpecularPower(30);
	lightTwo->SetFallOffDistance(60);

	lights[0] = lightZero;
	lights[1] = lightOne;
	lights[2] = lightTwo;

	//record light fallOffs
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		fallOffs[i] = lights[i]->GetFallOffDistance();
	}

	//create secondary render targets
	//shadow maps
	shadowMapZero = new RenderTexture(m_Direct3D->GetDevice(), SHADOWMAP_SIZE, SHADOWMAP_SIZE, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(shadowMapZero);
	shadowMapOne = new RenderTexture(m_Direct3D->GetDevice(), SHADOWMAP_SIZE, SHADOWMAP_SIZE, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(shadowMapOne);
	shadowMapTwo = new RenderTexture(m_Direct3D->GetDevice(), SHADOWMAP_SIZE, SHADOWMAP_SIZE, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(shadowMapTwo);

	shadowTargets[0] = shadowMapZero;
	shadowTargets[1] = shadowMapOne;
	shadowTargets[2] = shadowMapTwo;
	
	//2 downsampled targets for multiple post process passes
	glowTargetA = new RenderTexture(m_Direct3D->GetDevice(), screenWidth / 2, screenHeight / 2, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(glowTargetA);
	glowTargetB = new RenderTexture(m_Direct3D->GetDevice(), screenWidth / 2, screenHeight / 2, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(glowTargetB);
	//downsampled target to save previous frame's state
	previousGlow = new RenderTexture(m_Direct3D->GetDevice(), screenWidth / 2, screenHeight / 2, SCREEN_NEAR, SCREEN_DEPTH);
	//clear this to transparency
	previousGlow->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//create textures
	//planar fluid
	fluidTex = new Texture(m_Direct3D->GetDevice(), L"../res/flares.jpg");
	textures.push_back(fluidTex);
	//lights
	lightTex = new Texture(m_Direct3D->GetDevice(), L"../res/webtreats-2.png");
	textures.push_back(lightTex);
	//exploder
	exploderTex = new Texture(m_Direct3D->GetDevice(), L"../res/akumetsu_rain.JPG");
	textures.push_back(exploderTex);
	//skybox
	skyboxTex = new Texture(m_Direct3D->GetDevice(), L"../res/tileable-classic-nebula-space-patterns-6.jpg");
	textures.push_back(skyboxTex);

	//create meshes
	fluid = new QuadCPPatch(m_Direct3D->GetDevice(), L"../res/white.png", 10, 10);//10 quads of width 10
	meshes.push_back(fluid);
	lamp = new DoubleConeMesh(m_Direct3D->GetDevice(), L"../res/white.png", 20);
	meshes.push_back(lamp);
	exploder = new SphereTriList(m_Direct3D->GetDevice(), L"../res/white.png", 6);
	meshes.push_back(exploder);
	skybox = new Skybox(m_Direct3D->GetDevice(), L"../res/white.png", 6);
	meshes.push_back(skybox);
	//ortho mesh for post process
	glowFilter = new OrthoMesh(m_Direct3D->GetDevice(), screenWidth, screenHeight);
	meshes.push_back(glowFilter);
	
	//create shaders
	fluidS = new FluidShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(fluidS);
	textureS = new TextureShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(textureS);
	exploderS = new ExploderShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/exploder_gs.hlsl");
	shaders.push_back(exploderS);
	creationS = new ExploderShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/creation_gs.hlsl");
	shaders.push_back(creationS);
	destructionS = new ExploderShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/destruction_gs.hlsl");
	shaders.push_back(destructionS);
	hBlurS = new HorizontalBlurShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(hBlurS);
	vBlurS = new VerticalBlurShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(vBlurS);
	dimS = new DimShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(dimS);
	lavaLampS = new LavaLampShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(lavaLampS);
	fluidDepthS = new FluidDepthShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(fluidDepthS);
	exploderDepthS = new ExploderDepthShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/exploderDepth_gs.hlsl");
	shaders.push_back(exploderDepthS);
	creationDepthS = new ExploderDepthShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/creationDepth_gs.hlsl");
	shaders.push_back(creationDepthS);
	destructionDepthS = new ExploderDepthShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/destructionDepth_gs.hlsl");
	shaders.push_back(destructionDepthS);
	shadowFluidS = new ShadowFluidShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(shadowFluidS);

	//init time and timer
	timer = 20.0f;
	time = 0.0f;
}


Coursework::~Coursework()
{
	// Run base application deconstructor
	BaseApplication::~BaseApplication();
	
	//release shaders, meshes, textures and vectors
	for (int i = 0; i < shaders.size(); ++i)
	{
		if (shaders[i])
		{
			delete shaders[i];
			shaders[i] = 0;
		}
	}
	for (int i = 0; i < meshes.size(); ++i)
	{
		if (meshes[i])
		{
			delete meshes[i];
			meshes[i] = 0;
		}
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
		{
			delete textures[i];
			textures[i] = 0;
		}
	}
	for (int i = 0; i < renderTargets.size(); ++i)
	{
		if (renderTargets[i])
		{
			delete renderTargets[i];
			renderTargets[i] = 0;
		}
	}
	std::vector<BaseShader*>().swap(shaders);
	std::vector<BaseMesh*>().swap(meshes);
	std::vector<Texture*>().swap(textures);
	std::vector<RenderTexture*>().swap(renderTargets);
}

void Coursework::HandleInput(float frameTime)
{
	BaseApplication::HandleInput(frameTime);

	//wireframe on/off
	if (m_Input->isKeyDown(VK_SPACE))
	{
		if (wireframe)
		{
			m_Direct3D->TurnOffWireframe();
		}
		else
		{
			m_Direct3D->TurnOnWireframe();
		}
		m_Input->SetKeyUp(VK_SPACE);
		wireframe = !wireframe;		
	}

	//change selected light
	if (m_Input->isKeyDown('1'))
	{
		currentLight = 0;
		m_Input->SetKeyUp('1');
	}
	if (m_Input->isKeyDown('2'))
	{
		currentLight = 1;
		m_Input->SetKeyUp('2');
	}
	if (m_Input->isKeyDown('3'))
	{
		currentLight = 2;
		m_Input->SetKeyUp('3');
	}
	
	//toggle shadows on/off
	if (m_Input->isKeyDown(VK_CONTROL))
	{
		shadows = !shadows;
		m_Input->SetKeyUp(VK_CONTROL);
	}

	//toggle on/off selected light (off = falloff distance = 0)
	if (m_Input->isKeyDown(VK_TAB))
	{
		if (lights[currentLight]->GetFallOffDistance() != 0)
		{
			fallOffs[currentLight] = lights[currentLight]->GetFallOffDistance();
			lights[currentLight]->SetFallOffDistance(0);
		}
		else
			lights[currentLight]->SetFallOffDistance(fallOffs[currentLight]);

		m_Input->SetKeyUp(VK_TAB);
	}

	//control light positions of selected light with RTYFGH
	if (m_Input->isKeyDown('R'))// - Y
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y - m_Timer->GetTime()*10, lights[currentLight]->GetPosition().z);
	if (m_Input->isKeyDown('T'))// + Z
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z + m_Timer->GetTime() * 10);
	if (m_Input->isKeyDown('Y'))// + Y
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y + m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().z);
	if (m_Input->isKeyDown('F'))// - X
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x - m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z);
	if (m_Input->isKeyDown('G'))// - Z
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z - m_Timer->GetTime() * 10);
	if (m_Input->isKeyDown('H'))// + X
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x + m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z);

	//control exploder state
	if (m_Input->isKeyDown(VK_RETURN))
	{
		//increase state and reset timer
		exploderState++;
		int state = exploderState % 3;
		if (state == 0)
			timer = 15.0f;
		else if (state == 2)
			timer = 10.0f;
		m_Input->SetKeyUp(VK_RETURN);
	}

	//if shift increase variables, if no shift decrease
	if (m_Input->isKeyDown(VK_SHIFT))
	{
		//control tessellation density
		if (m_Input->isKeyDown('Z') && tessellation < 64)
		{
			tessellation += m_Timer->GetTime() * 10;
			if (tessellation > 64)
				tessellation = 64;
		}
		//control tiling density
		if (m_Input->isKeyDown('X'))
		{
			tiling += m_Timer->GetTime() * 10;
		}
		//control wave frequency
		if (m_Input->isKeyDown('C'))
		{
			frequency += m_Timer->GetTime() / 5;
		}
		//control wave height
		if (m_Input->isKeyDown('V'))
		{
			waveHeight += m_Timer->GetTime() * 2;
		}
		//control dim strength
		if (m_Input->isKeyDown('B') && dimStrength < 1.5)
		{
			dimStrength += m_Timer->GetTime();
		}
		//control falloff distance of current light
		if (m_Input->isKeyDown('N'))
			lights[currentLight]->SetFallOffDistance(lights[currentLight]->GetFallOffDistance() + 10 * m_Timer->GetTime());		
	}
	else
	{
		//control tessellation density
		if (m_Input->isKeyDown('Z') && tessellation > 1)
		{
			tessellation -= m_Timer->GetTime() * 10;
			if (tessellation < 1)
				tessellation = 1;
		}
		//control tiling density
		if (m_Input->isKeyDown('X') && tiling > 0)
		{
			tiling -= m_Timer->GetTime() * 10;
			if (tiling < 0)
				tiling = 0;
		}
		//control wave frequency
		if (m_Input->isKeyDown('C'))
		{
			frequency -= m_Timer->GetTime() / 5;
		}
		//control wave height
		if (m_Input->isKeyDown('V') && waveHeight > 0)
		{
			waveHeight -= m_Timer->GetTime() * 2;
			if (waveHeight < 0)
				waveHeight = 0;
		}
		//control dim strength
		if (m_Input->isKeyDown('B') && dimStrength > 0.4)
		{
			dimStrength -= m_Timer->GetTime();
			if (dimStrength < 0.4)
				dimStrength = 0.4;
		}
		//control falloff distance of current light if light is on
		if (m_Input->isKeyDown('N') && (lights[currentLight]->GetFallOffDistance() > 0))
		{		
			lights[currentLight]->SetFallOffDistance(lights[currentLight]->GetFallOffDistance() - 10 * m_Timer->GetTime());
			if (lights[currentLight]->GetFallOffDistance() < 0)
			{
				lights[currentLight]->SetFallOffDistance(0);
			}
		}
	}
}

bool Coursework::Frame()
{
	bool result;

	result = BaseApplication::Frame();
	if (!result)
	{
		return false;
	}

	//update vars
	time += m_Timer->GetTime();

	// Render the graphics.
	result = Render();
	if (!result)
	{
		return false;
	}

	return true;
}

bool Coursework::Render()
{
	XMMATRIX worldMatrix, viewMatrix, projectionMatrix, orthoMatrix, baseViewMatrix;;

	// Clear the scene.
	m_Direct3D->BeginScene(0.1f, 0.1f, 0.1f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Camera->GetViewMatrix(viewMatrix);
	m_Direct3D->GetProjectionMatrix(projectionMatrix);
	//get ortho matrix and base view matrix for 2d rendering
	m_Direct3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetBaseViewMatrix(baseViewMatrix);

	if (shadows)
	{
		//create shadow map
		createShadowMaps(&worldMatrix);
	}

	//render skybox
	m_Direct3D->TurnZBufferOff();
	//translate to camera position
	worldMatrix = DirectX::XMMatrixTranslation(m_Camera->GetPosition().x, m_Camera->GetPosition().y, m_Camera->GetPosition().z);
	//render skybox with texture shader
	skybox->SendData(m_Direct3D->GetDeviceContext());
	textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, skyboxTex->GetTexture());
	textureS->Render(m_Direct3D->GetDeviceContext(), skybox->GetIndexCount());
	//reset world matrix
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Direct3D->TurnZBufferOn();

	if (shadows)//render lighting with shadows on fluid
	{
		//render planar fluid
		fluid->SendData(m_Direct3D->GetDeviceContext());
		shadowFluidS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, fluidTex->GetTexture(),
			lights, m_Camera, frequency, time * 5, tessellation, 10 * 10, tiling, waveHeight , shadowMaps);//size of patch: 10quads*10width = 100
		shadowFluidS->Render(m_Direct3D->GetDeviceContext(), fluid->GetIndexCount());
	}
	else//render lighting without shadows on fluid
	{
		//render planar fluid
		fluid->SendData(m_Direct3D->GetDeviceContext());
		fluidS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, fluidTex->GetTexture(),
			lights, m_Camera, frequency, time * 5, tessellation, 10 * 10, tiling, waveHeight);//size of patch: 10quads*10width = 100
		fluidS->Render(m_Direct3D->GetDeviceContext(), fluid->GetIndexCount());
	}
	

	//post process lamps to create glow
	//render lights to texture
	lightsToTexture(&worldMatrix, &viewMatrix, &projectionMatrix);
	//Turn off the Z buffer to begin all 2D rendering.
	m_Direct3D->TurnZBufferOff();
	//turn on alpha blending
	m_Direct3D->TurnOnAlphaBlending();
	//blend with previous frame and save result for next iteration
	addGlowTrail(&worldMatrix, &baseViewMatrix, &orthoMatrix);
	//apply horizontal blur
	hBlur(&worldMatrix, &baseViewMatrix, &orthoMatrix);
	//apply vertical blur
	vBlur(&worldMatrix, &baseViewMatrix, &orthoMatrix);
	//render final filter on top of the scene
	drawGlowFilter(&worldMatrix, &baseViewMatrix, &orthoMatrix);
	//turn alpha blending back off
	m_Direct3D->TurnOffAlphaBlending();
	//now that all 2d rendering is done Turn the Z buffer back on
	m_Direct3D->TurnZBufferOn();

	//render and animate exploder depending on state
	renderExploder(&worldMatrix, &viewMatrix, &projectionMatrix);

	//render lamps
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		//translate to light position and add some for periodic floating
		worldMatrix = DirectX::XMMatrixTranslation(lights[i]->GetPosition().x, lights[i]->GetPosition().y + cos(time) * 2, lights[i]->GetPosition().z);		
		//send geometry data and render with texture shader
		lamp->SendData(m_Direct3D->GetDeviceContext());
		lavaLampS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, lightTex->GetTexture(), lights[i]->GetDiffuseColour(), time);
		lavaLampS->Render(m_Direct3D->GetDeviceContext(), lamp->GetIndexCount());
		//reset world matrix
		m_Direct3D->GetWorldMatrix(worldMatrix);
	}

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();
	return true;
}

void Coursework::renderExploder(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix)
{
	//render and animate exploder depending on state
	timer -= m_Timer->GetTime() * 5;
	switch (exploderState % 3)
	{
	case 0://creation
		if (timer > 0)
		{
			//translate to center of fluid plane
			*worldMatrix = DirectX::XMMatrixTranslation(50, 20, 50);
			//send geometry data and render with shader
			exploder->SendData(m_Direct3D->GetDeviceContext());
			creationS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix,
				exploderTex->GetTexture(), lights, m_Camera, timer);
			creationS->Render(m_Direct3D->GetDeviceContext(), exploder->GetIndexCount());
			//reset world matrix
			m_Direct3D->GetWorldMatrix(*worldMatrix);
		}
		else
			exploderState++;//increase state at end of animation
		break;

	case 1://stable
		//turn off back culling to see triangles from all sides
		m_Direct3D->TurnOffBackCulling(wireframe);
		//translate to center of fluid plane
		*worldMatrix = DirectX::XMMatrixTranslation(50, 20, 50);
		//send geometry data and render with shader
		exploder->SendData(m_Direct3D->GetDeviceContext());
		exploderS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix,
			exploderTex->GetTexture(), lights, m_Camera, timer);
		exploderS->Render(m_Direct3D->GetDeviceContext(), exploder->GetIndexCount());
		//reset world matrix
		m_Direct3D->GetWorldMatrix(*worldMatrix);
		//turn on back culling
		m_Direct3D->TurnOnBackCulling(wireframe);
		break;

	case 2://destruction
		if (timer > 0)
		{
			//translate to center of fluid plane
			*worldMatrix = DirectX::XMMatrixTranslation(50, 20, 50);
			//send geometry data and render with shader
			exploder->SendData(m_Direct3D->GetDeviceContext());
			destructionS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix,
				exploderTex->GetTexture(), lights, m_Camera, timer);
			destructionS->Render(m_Direct3D->GetDeviceContext(), exploder->GetIndexCount());
			//reset world matrix
			m_Direct3D->GetWorldMatrix(*worldMatrix);
		}
		break;

	default:
		//not happening
		break;
	}
}

void Coursework::lightsToTexture(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix)
{
	// Set the render target to be the downsampled target A
	glowTargetA->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetA->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);
	
	//render lamps
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		//translate to light position and add periodic floating
		*worldMatrix = DirectX::XMMatrixTranslation(lights[i]->GetPosition().x, lights[i]->GetPosition().y + cos(time) * 2, lights[i]->GetPosition().z);

		//send geometry data and render with texture shader
		lamp->SendData(m_Direct3D->GetDeviceContext());
		lavaLampS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix, lightTex->GetTexture(), lights[i]->GetDiffuseColour(), time);
		lavaLampS->Render(m_Direct3D->GetDeviceContext(), lamp->GetIndexCount());
		//reset world matrix
		m_Direct3D->GetWorldMatrix(*worldMatrix);
	}
}

void Coursework::addGlowTrail(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//the render target is still glowTargetA
	//dim the previous frame and blend it with the current one
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	dimS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		previousGlow->GetShaderResourceView(), dimStrength);
	dimS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());

	//Save the current frame
	//Set the render target to be the previousGlow render to texture.
	previousGlow->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//save current frame with texture shader
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView());
	textureS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::hBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//Set the render target to be the down sampled render to texture B.
	glowTargetB->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetB->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//render glowTargetA on glowTargetB applying horizontal blur
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	hBlurS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView(), sWidth);
	hBlurS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::vBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//Set the render target to be the down sampled render to texture A.
	glowTargetA->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetA->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//render glowTargetA on glowTargetB applying horizontal blur
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	vBlurS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetB->GetShaderResourceView(), sWidth);
	vBlurS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::drawGlowFilter(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//reset screen size
	m_Direct3D->ResetViewport();
	//set render target to back buffer
	m_Direct3D->SetBackBufferRenderTarget();
	//render final filter to screen
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView());
	textureS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::createShadowMaps(XMMATRIX* worldMatrix)
{
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		//get the light's view and projection matrices
		XMMATRIX lightViewMatrix, lightProjectionMatrix;
		lights[i]->GenerateViewMatrix();
		lightViewMatrix = lights[i]->GetViewMatrix();
		lights[i]->GenerateProjectionMatrix(1.0f, 100.0f);
		lightProjectionMatrix = lights[i]->GetProjectionMatrix();

		shadowTargets[i]->SetRenderTarget(m_Direct3D->GetDeviceContext());
		shadowTargets[i]->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.0f, 0.0f, 0.0f, 1.0f);

		//render the scene from the light's point of view
		//render planar fluid depth info
		fluid->SendData(m_Direct3D->GetDeviceContext());
		fluidDepthS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, lightViewMatrix, lightProjectionMatrix,
			frequency, time * 5, tessellation, 10 * 10, waveHeight);
		fluidDepthS->Render(m_Direct3D->GetDeviceContext(), fluid->GetIndexCount());
		//render exploder depth info
		renderExploderDepth(worldMatrix, &lightViewMatrix, &lightProjectionMatrix);

		//save new shader resource view to sentd it to the shadow shader
		shadowMaps[i] = shadowTargets[i]->GetShaderResourceView();
	}
	
	//set render target back to screen
	m_Direct3D->SetBackBufferRenderTarget();
	//reset the viewport since shadow map is downsampled
	m_Direct3D->ResetViewport();
}

void Coursework::renderExploderDepth(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix)
{
	//render and animate exploder depending on state
	//timer -= m_Timer->GetTime() * 5;
	switch (exploderState % 3)
	{
	case 0://creation
		if (timer > 0)
		{
			//translate to center of fluid plane
			*worldMatrix = DirectX::XMMatrixTranslation(50, 20, 50);
			//send geometry data and render with shader
			exploder->SendData(m_Direct3D->GetDeviceContext());
			creationDepthS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix, timer);
			creationDepthS->Render(m_Direct3D->GetDeviceContext(), exploder->GetIndexCount());
			//reset world matrix
			m_Direct3D->GetWorldMatrix(*worldMatrix);
		}
		else
			exploderState++;//increase state at end of animation
		break;

	case 1://stable
		//turn off back culling to see triangles from all sides
		m_Direct3D->TurnOffBackCulling(wireframe);
		//translate to center of fluid plane
		*worldMatrix = DirectX::XMMatrixTranslation(50, 20, 50);
		//send geometry data and render with shader
		exploder->SendData(m_Direct3D->GetDeviceContext());
		exploderDepthS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix, timer);
		exploderDepthS->Render(m_Direct3D->GetDeviceContext(), exploder->GetIndexCount());
		//reset world matrix
		m_Direct3D->GetWorldMatrix(*worldMatrix);
		//turn on back culling
		m_Direct3D->TurnOnBackCulling(wireframe);
		break;

	case 2://destruction
		if (timer > 0)
		{
			//translate to center of fluid plane
			*worldMatrix = DirectX::XMMatrixTranslation(50, 20, 50);
			//send geometry data and render with shader
			exploder->SendData(m_Direct3D->GetDeviceContext());
			destructionDepthS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix, timer);
			destructionDepthS->Render(m_Direct3D->GetDeviceContext(), exploder->GetIndexCount());
			//reset world matrix
			m_Direct3D->GetWorldMatrix(*worldMatrix);
		}
		break;

	default:
		//not happening
		break;
	}
}