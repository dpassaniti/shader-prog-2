#pragma once
#include "BaseMesh.h"

using namespace DirectX;

class Skybox : public BaseMesh
{

public:
	Skybox(ID3D11Device* device, WCHAR* textureFilename, int resolution = 20);
	~Skybox();

protected:
	void InitBuffers(ID3D11Device* device);
	int m_resolution;
};
