#pragma once
#include "BaseShader.h"
#include "Light.h"
#include "Camera.h"

using namespace std;
using namespace DirectX;

class ExploderDepthShader : public BaseShader
{
	struct ManipBuffer//geometry and vertex manipulation;
	{
		float time;
		XMFLOAT3 padding;
	};

public:
	ExploderDepthShader(ID3D11Device* device, HWND hwnd, WCHAR* gsPath);//path to geometry shader for 3 different animations
	~ExploderDepthShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, float time);
	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
	void InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* m_matrixBuffer;
	ID3D11Buffer* manipBfr;
};