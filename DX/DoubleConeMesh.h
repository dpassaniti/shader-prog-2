#pragma once
#include "BaseMesh.h"

using namespace DirectX;

class DoubleConeMesh : public BaseMesh
{

public:
	DoubleConeMesh(ID3D11Device* device, WCHAR* textureFilename, int resolution);
	~DoubleConeMesh();

protected:
	void InitBuffers(ID3D11Device* device);
	int m_resolution;
};