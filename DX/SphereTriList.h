//sphere described by triangle list for use with geometry shader
#pragma once
#include "BaseMesh.h"

using namespace DirectX;

class SphereTriList : public BaseMesh
{

public:
	SphereTriList(ID3D11Device* device, WCHAR* textureFilename, int resolution);
	~SphereTriList();

	void SendData(ID3D11DeviceContext*);

protected:
	void InitBuffers(ID3D11Device* device);
	int m_resolution;

};