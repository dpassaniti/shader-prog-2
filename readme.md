Demo showcasing use of vertex, hull, domain, geometry and pixel shader stages and different rendering techniques such as dynamic lighting, shadowmapping and glow post processing.

- Video: https://www.youtube.com/watch?v=ME7IKhu-PAE
